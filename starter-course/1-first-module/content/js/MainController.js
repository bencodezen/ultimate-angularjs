function MainController($scope) {
	$scope.name = 'Ben';
}

angular
	.module('app')
	.controller('MainController', MainController);
