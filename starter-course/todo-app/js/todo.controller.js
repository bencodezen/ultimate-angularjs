function ToDoController() {
	this.newToDo = '';
	this.list = [{
		title: 'First to do item',
		completed: false
	}, {
		title: 'Second to do item',
		completed: false
	}, {
		title: 'Third to do item',
		completed: false
	}];
	this.addToDo = function() {
		this.list.unshift({
			title: this.newToDo,
			completed: false
		});

		this.newToDo = '';
	};
	this.removeToDo = function(item, index) {
		this.list.splice(index, 1);
	};
	this.getRemaining = function() {
		return this.list.filter(function(item) {
			return !item.completed;
		});
	};
}

angular
	.module('app')
	.controller('ToDoController', ToDoController);